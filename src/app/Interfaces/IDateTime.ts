export default interface IDateTime {
    dateMasihi: string,
    dateHijrah: string,
    Day: string,
    Imsak: string;
    Subuh : string,
    Syuruk: string,
    Duha: string,
    Zuhur: string,
    Asar: string,
    Maghrib: string,
    Isya: string
}