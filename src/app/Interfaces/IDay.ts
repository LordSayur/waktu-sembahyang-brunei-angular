export default interface IDay {
    dateMasihi: string,
    dateHijrah: string,
    Day: string,
    Imsak: string,
    Subuh: string,
    Duha: string,
    Zuhur: string,
    Asar: string,
    Maghrib: string,
    Isya: string
}