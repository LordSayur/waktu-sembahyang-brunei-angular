export default interface IPrayer {
    Imsak: boolean,
    Subuh: boolean,
    Syuruk: boolean,
    Zuhur: boolean,
    Duha: boolean,
    Asar: boolean,
    Maghrib: boolean,
    Isya: boolean,
    Malam: boolean
}