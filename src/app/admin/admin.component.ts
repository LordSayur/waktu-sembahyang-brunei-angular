import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as M from '../../assets/materialize/js/materialize.min.js';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

    textOriginal: string;
    textPreview: string[];
    days:any[] = [];
    month: string;
    result: string;
    options = {};
    value: number;

  constructor(private db: AngularFirestore) { }

  ngOnInit() {
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems, this.options);
  }

  textToArrayofObject():void {
      this.days = []
    let textLineArray: string[] = this.textLineToArray();
    
    textLineArray.forEach(textLine =>{
        let texts: string[] = this.textToArray(textLine);
        let day = {
                Date: "",
                Tarikh: "",
                Imsak: "",
                Subuh: "",
                Syuruk: "",
                Duha: "",
                Zuhur: "",
                Asar: "",
                Maghrib: "",
                Isya: ""
            };
        day.Date = texts[0];
        day.Tarikh = texts[1];
        day.Imsak = texts[2];
        day.Subuh = texts[3];
        day.Syuruk = texts[4];
        day.Duha = texts[5];
        day.Zuhur = texts[6];
        day.Asar = texts[7];
        day.Maghrib = texts[8];
        day.Isya = texts[9];
        this.days.push(day);
    })
    console.log(this.days);
    this.result = JSON.stringify(this.days);
  }

  textLineToArray(): string[] {
    let txtOriginal = this.textOriginal;
    let result = txtOriginal.split("\n");
    return result;
  }

  textToArray(textLine:string): string[] {
    let str = textLine;
    let result = str.split("\t");
    return result;
  }

  Add():void {
    // this.db.collection("waktu").doc("4").delete();
    // this.db.collection("waktu").add("4");
    this.db.collection("waktu").doc(`${this.value}`).set(
      {
        Day : this.days
      }
    )
    this.result = "Uploaded!";
  }

}
