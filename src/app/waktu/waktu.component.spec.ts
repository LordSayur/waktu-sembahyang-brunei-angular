import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaktuComponent } from './waktu.component';

describe('WaktuComponent', () => {
  let component: WaktuComponent;
  let fixture: ComponentFixture<WaktuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaktuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaktuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
