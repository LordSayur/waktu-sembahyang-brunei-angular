import { Component, OnInit } from '@angular/core';
import IDay from '../Interfaces/IDay.js';
import IPrayer from '../Interfaces/IPrayer.js';
import IDateTime from '../Interfaces/IDateTime.js';
import { AngularFirestore } from '@angular/fire/firestore';
import * as M from '../../assets/materialize/js/materialize.min.js';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-waktu',
  templateUrl: './waktu.component.html',
  styleUrls: ['./waktu.component.scss']
})
export class WaktuComponent implements OnInit {

  title = 'waktu-sembahyang-brunei-angular';
  options = 
  [
    {
      fullWidth: true,
      indicators: true
    },
    {

    }
  ]

  prayerTime: IPrayer = {
    Imsak: false,
    Subuh: false,
    Duha: false,
    Syuruk: false,
    Zuhur: false,
    Asar: false,
    Maghrib: false,
    Isya: false,
    Malam: false
  }

  dtToday: IDateTime = {
    dateMasihi : "",
    dateHijrah: "",
    Day: "",
    Imsak: "",
    Subuh : "",
    Syuruk: "",
    Duha: "",
    Zuhur: "",
    Asar: "",
    Maghrib: "",
    Isya: ""
  }

  dtTomorrow: IDateTime = {
    dateMasihi : "",
    dateHijrah: "",
    Day: "",
    Imsak: "",
    Subuh : "",
    Syuruk: "",
    Duha: "",
    Zuhur: "",
    Asar: "",
    Maghrib: "",
    Isya: ""
  }

  dtDayAfterTomorrow: IDateTime = {
    dateMasihi : "",
    dateHijrah: "",
    Day: "",
    Imsak: "",
    Subuh : "",
    Syuruk: "",
    Duha: "",
    Zuhur: "",
    Asar: "",
    Maghrib: "",
    Isya: ""
  }

  today: IDay = {
    dateMasihi : "",
    dateHijrah: "",
    Day: "",
    Imsak: "",
    Subuh : "",
    Duha: "",
    Zuhur: "",
    Asar: "",
    Maghrib: "",
    Isya: ""
  };

  tomorrow: IDay = {
    dateMasihi : "",
    dateHijrah: "",
    Day: "",
    Imsak: "",
    Subuh : "",
    Duha: "",
    Zuhur: "",
    Asar: "",
    Maghrib: "",
    Isya: ""
  };

  dayAfterTomorrow: IDay = {
    dateMasihi : "",
    dateHijrah: "",
    Day: "",
    Imsak: "",
    Subuh : "",
    Duha: "",
    Zuhur: "",
    Asar: "",
    Maghrib: "",
    Isya: ""
  };

  months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  days = ["Isnin", "Selasa", "Rabu", "Khamis", "Jumaat", "Sabtu", "Ahad"];
  date = new Date();
  day = this.date.getDate();
  monthNum = this.date.getMonth();
  month = this.months[this.monthNum];
  year = this.date.getFullYear();

  currentTime: number;
  activeClass: string;
  district:string = "Brunei Muara & Temburong";
  districtNum: number = 1;
  countDown: number;
  countDownText: string;
  almost: boolean = false;

  Month:any = [];

  constructor(private db: AngularFirestore, private cookieService: CookieService) {
    this.Month = db.collection("waktu").snapshotChanges().subscribe( serverItems => {
      this.Month = [];
      serverItems.forEach(a => {
        let item :any = a.payload.doc.data();
        this.Month.push(item);
      });
      this.UpdateAllText();
      this.UpdateView(this.today, this.dtToday);
      this.UpdateView(this.tomorrow, this.dtTomorrow);
      this.UpdateView(this.dayAfterTomorrow, this.dtDayAfterTomorrow);
      var ayam = setInterval(() => { this.updateHighlight(); }, 1000);
    });
  }

  ngOnInit()
  {
    var carousel = document.querySelectorAll('.carousel');
    var instances = M.Carousel.init(carousel, this.options[0]);
    var dropdown = document.querySelectorAll('.dropdown-trigger');
    var instances = M.Dropdown.init(dropdown);

    if (this.cookieService.check("districtNum")) {
      this.districtNum = parseInt(this.cookieService.get("districtNum"));
      this.selectDistrict(this.districtNum);
    }
    else{
      this.districtNum = 1;
      this.selectDistrict(this.districtNum);
    }
  }
  
  private UpdateAllText() {
    this.dtToday.dateMasihi = this.day + " " + this.month + " " + this.year;
    this.UpdateText(this.dtToday, 0, this.day - 1);
    // check if the day after tomorrow is 1st day of the next month
    if (typeof this.Month[this.monthNum].Day[this.day] === 'undefined') {
      // tomorrow is 1st day of next month
      this.dtTomorrow.dateMasihi = 1 + " " + this.months[this.monthNum + 1] + " " + this.year;
      this.UpdateText(this.dtTomorrow, 1, 0);
      // the day after tomorrow is 2nd day of the next month
      this.dtDayAfterTomorrow.dateMasihi = 2 + " " + this.months[this.monthNum + 1] + " " + this.year;
      this.UpdateText(this.dtDayAfterTomorrow, 1, 1);
    }
    else {
      // tomorrow is last day of this month
      this.dtTomorrow.dateMasihi = this.day + 1 + " " + this.month + " " + this.year;
      this.UpdateText(this.dtTomorrow, 0, this.day);
      // check if the day after tomorrow is 1st day of the next month
      if (typeof this.Month[this.monthNum].Day[this.day + 1] === 'undefined') {
        // the day after tomorrow is 1st day of the next month
        this.dtDayAfterTomorrow.dateMasihi = 1 + " " + this.months[this.monthNum + 1] + " " + this.year;
        this.UpdateText(this.dtDayAfterTomorrow, 1, 0);
      }
      else {
        // the day after tomorrow is the last day of the this month
        this.dtDayAfterTomorrow.dateMasihi = this.day + 2 + " " + this.month + " " + this.year;
        this.UpdateText(this.dtDayAfterTomorrow, 0, this.day + 1);
      }
    }
    this.dtToday.Day = this.days[this.date.getDay() - 1];
    if (this.date.getDay() - 1 < 5) {
      this.dtTomorrow.Day = this.days[this.date.getDay()];
      this.dtDayAfterTomorrow.Day = this.days[this.date.getDay() + 1];
    }
    else if (this.date.getDay() - 1 == 5) {
      this.dtTomorrow.Day = this.days[this.date.getDay()];
      this.dtDayAfterTomorrow.Day = this.days[0];
    }
    else if (this.date.getDay() - 1 === 6) {
      this.dtTomorrow.Day = this.days[0];
      this.dtDayAfterTomorrow.Day = this.days[1];
    }
    else {
      this.dtTomorrow.Day = 'hmmmm';
      this.dtDayAfterTomorrow.Day = 'hmmmm';
    }
  }

  private UpdateText(iDtDay:IDateTime, monthOffset:number, dayOffset:number) {
    iDtDay.dateHijrah = this.Month[this.monthNum + monthOffset].Day[dayOffset].Tarikh;
    iDtDay.Imsak = this.districtCheck(this.Month[ this.monthNum + monthOffset ].Day[ dayOffset ].Imsak);
    iDtDay.Subuh = this.districtCheck(this.Month[ this.monthNum + monthOffset ].Day[ dayOffset ].Subuh);
    iDtDay.Syuruk = this.districtCheck(this.Month[ this.monthNum + monthOffset ].Day[ dayOffset ].Syuruk);
    iDtDay.Duha = this.districtCheck(this.Month[ this.monthNum + monthOffset ].Day[ dayOffset ].Duha);
    iDtDay.Zuhur = this.districtCheck(this.Month[ this.monthNum + monthOffset ].Day[ dayOffset ].Zuhur);
    iDtDay.Asar = this.districtCheck(this.Month[ this.monthNum + monthOffset ].Day[ dayOffset ].Asar);
    iDtDay.Maghrib = this.districtCheck(this.Month[ this.monthNum + monthOffset ].Day[ dayOffset ].Maghrib);
    iDtDay.Isya = this.districtCheck(this.Month[ this.monthNum + monthOffset ].Day[ dayOffset ].Isya);
  }

  private UpdateView(iDay: IDay, iDtDay:IDateTime) {
    iDay.dateMasihi = iDtDay.dateMasihi;
    iDay.dateHijrah = iDtDay.dateHijrah;
    iDay.Day = iDtDay.Day;
    iDay.Imsak = iDtDay.Imsak + " am";
    iDay.Subuh = iDtDay.Subuh + " am";
    iDay.Duha = iDtDay.Duha + " am";
    iDay.Zuhur = iDtDay.Zuhur + " pm";
    iDay.Asar = iDtDay.Asar + " pm";
    iDay.Maghrib = iDtDay.Maghrib + " pm";
    iDay.Isya = iDtDay.Isya + " pm";
  }

  private updateHighlight():void 
  {
    this.currentTime = new Date().getHours() + new Date().getMinutes() / 100;
    // console.log(this.currentTime);
    
    this.activeClass = "active green-indicator";

    if (parseFloat(this.dtToday.Imsak) > this.currentTime) 
    {
      this.prayerTime.Isya = true;
      this.prayerTime.Maghrib = false;
      this.prayerTime.Malam = false;
      this.updateCountDown(parseFloat(this.dtToday.Imsak), this.currentTime,"lagi kan masuk waktu Imsak");
      // console.log("Isya!!!");
    }
    else if (parseFloat(this.dtToday.Subuh) > this.currentTime) 
    {
      this.prayerTime.Imsak = true;
      this.prayerTime.Isya = false;
      this.prayerTime.Malam = false;
      this.updateCountDown(parseFloat(this.dtToday.Subuh), this.currentTime,"lagi kan masuk waktu Subuh");
      // console.log("Imsak!!!");
    }
    else if (parseFloat(this.dtToday.Syuruk) > this.currentTime) 
    {
      this.prayerTime.Imsak = false;
      this.prayerTime.Subuh = true;
      this.updateCountDown(parseFloat(this.dtToday.Syuruk), this.currentTime,"lagi kan abis waktu Subuh");
      // console.log("Subuh!!!");
    }
    else if (parseFloat(this.dtToday.Duha) > this.currentTime) 
    {
      this.prayerTime.Subuh = false;
      this.prayerTime.Syuruk = true;
      this.updateCountDown(parseFloat(this.dtToday.Duha), this.currentTime,"lagi kn masuk waktu Duha");
      // console.log("Syuruk!!!");
    }
    else if (parseFloat(this.dtToday.Zuhur) - 0.10 > this.currentTime) 
    {
      this.prayerTime.Syuruk = false;
      this.prayerTime.Duha = true;
      this.updateCountDown(parseFloat(this.dtToday.Zuhur), this.currentTime,"lagi kn masuk waktu Zuhur");
      // console.log("Duha!!!");
    }
    else if (parseFloat(this.dtToday.Zuhur) > this.currentTime) 
    {
      this.prayerTime.Syuruk = false;
      this.prayerTime.Duha = false;
      this.updateCountDown(parseFloat(this.dtToday.Zuhur), this.currentTime,"lagi kn masuk waktu Zuhur");
      // console.log("Pre-Zuhur!!!");
    }
    else if (parseFloat(this.dtToday.Asar) + 12.0 > this.currentTime) 
    {
      this.prayerTime.Duha = false;
      this.prayerTime.Zuhur = true;
      this.updateCountDown(parseFloat(this.dtToday.Asar) + 12.0, this.currentTime,"lagi kn masuk waktu Asar");
      // console.log("Zuhur!!!");
    }
    else if (parseFloat(this.dtToday.Maghrib) + 12.0 > this.currentTime) 
    {
      this.prayerTime.Zuhur = false;
      this.prayerTime.Asar = true;
      this.updateCountDown(parseFloat(this.dtToday.Maghrib) + 12.0, this.currentTime,"lagi kn masuk waktu Maghrib");
      // console.log("Asar!!!");
    }
    else if (parseFloat(this.dtToday.Isya) + 12.0 > this.currentTime) 
    {
      this.prayerTime.Asar = false;
      this.prayerTime.Maghrib = true;
      this.prayerTime.Malam = true;
      this.today.dateHijrah = this.tomorrow.dateHijrah;
      this.updateCountDown(parseFloat(this.dtToday.Isya) + 12.0, this.currentTime,"lagi kn masuk waktu Isya");
      // console.log("Maghrib!!!");
    }
    else if (12.0 + 12.0 > this.currentTime) 
    {
      this.prayerTime.Maghrib = false;
      this.prayerTime.Isya = true;
      this.prayerTime.Malam = true;
      this.today.dateHijrah = this.tomorrow.dateHijrah;
      this.updateCountDown(this.dtToday.Imsak, this.currentTime,"Assalamualaikum");
      this.countDown = null;
      // console.log("Isya!!!");
    }
    else 
    {
      console.log("hmmmm...");
    }
  }

  private updateCountDown(nextPrayerTime,currentTime, text){
    currentTime = currentTime.toString().split(".");
    nextPrayerTime = nextPrayerTime.toFixed(2)
    nextPrayerTime = nextPrayerTime.toString().split(":");
    if (typeof currentTime[1] === 'undefined') {
      currentTime[1] = "00";
    }
    if (currentTime[1].length == 1) {
      currentTime[1] = currentTime[1] + 0;
    }
    if (nextPrayerTime[1].length == 1) {
      nextPrayerTime[1] = nextPrayerTime[1] + 0;
    }
    let diffHour = parseInt(nextPrayerTime[0]) - parseInt(currentTime[0]);
    let diffMin = parseInt(nextPrayerTime[1]) + (60 * diffHour) - parseInt(currentTime[1]);
    this.countDown = diffMin;
    // update count down highlight
    if (this.countDown < 16) {
      this.almost = true;
    }
    else{
      this.almost = false;
    }
    // update count down text
    this.countDownText = text;
  }

  selectDistrict(district:number) {
    switch (district) {
      case 1:
        this.district = "Brunei Muara & Temburong";
        this.districtNum = 1;
        this.cookieService.set("districtNum","1");
        break;

      case 2:
        this.district = "Tutong";
        this.districtNum = 2;
        this.cookieService.set("districtNum","2");
        break;

      case 3:
        this.district = "Belait";
        this.districtNum = 3;
        this.cookieService.set("districtNum","3");
        break;
    
      default:
        this.district = "Brunei Muara & Temburong";
        this.districtNum = 1;
        this.cookieService.set("districtNum","1");
        break;
    }
    this.UpdateAllText();
    this.UpdateView(this.today, this.dtToday);
    this.UpdateView(this.tomorrow, this.dtTomorrow);
    this.UpdateView(this.dayAfterTomorrow, this.dtDayAfterTomorrow);
    this.prayerTime = {
      Imsak: false,
      Subuh: false,
      Duha: false,
      Syuruk: false,
      Zuhur: false,
      Asar: false,
      Maghrib: false,
      Isya: false,
      Malam: false
    }
    this.updateHighlight();
  }

  districtCheck(time:string):string{
    let result:string;
    switch (this.districtNum) {
      case 1:
        return time;

      case 2:
        
        result = this.aboveSixtyCheck(time, result, 1);
        return result;

      case 3:
      result = this.aboveSixtyCheck(time, result, 3);
      return result;
    
      default:
        return time;
    }
  }

  private aboveSixtyCheck(time: string, result: string, timeOffset:number) {
    let timeMod = time.split(":");
    let resultTimeMin = parseInt(timeMod[1]) + timeOffset;
    let resultTimeHour = parseFloat(timeMod[0]);
    if (resultTimeMin > 59) {
      resultTimeMin = resultTimeMin - 60;
      resultTimeHour = resultTimeHour + 1;
      if (resultTimeHour > 9) {
        result = resultTimeHour.toString() + ":0" + resultTimeMin.toString();
      }
      else{
        result = "0" + resultTimeHour.toString() + ":0" + resultTimeMin.toString();
      }
    }
    else {
      if (resultTimeHour > 9) {
        result = resultTimeHour.toString() + ":" + resultTimeMin.toString();
      }
      else{
        result = "0" + resultTimeHour.toString() + ":" + resultTimeMin.toString();
      }
      // result = resultTimeHour.toString() + "." + resultTimeMin.toString();
    }
    return result;
  }
}
