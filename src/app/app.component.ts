import { Component, OnInit } from '@angular/core';
import * as M from '../assets/materialize/js/materialize.min.js';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit 
{
  title = 'waktu-sembahyang-brunei-angular';
  options = 
  {
    fullWidth: true,
    indicators: true
  }
  version: string;

  constructor() {
  }

  ngOnInit()
  {
    // Materialize CSS Init
    var elems = document.querySelectorAll('.carousel');
    var instances = M.Carousel.init(elems, this.options);

    this.version = "Lord Sayur v2.6.6";
  }
}
