import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { ServiceWorkerModule } from '@angular/service-worker';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { CookieService } from 'ngx-cookie-service';

import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { AdminComponent } from './admin/admin.component';
import { WaktuComponent } from './waktu/waktu.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    WaktuComponent
  ],
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    RouterModule.forRoot([
      {path:'admin', component: AdminComponent},
      {path:'', component: WaktuComponent},
      {path:'**', redirectTo: '', pathMatch:'full'}
    ]),
    FormsModule
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
