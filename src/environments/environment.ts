// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBqFq91f9216GkBFVSAzFG1Qfck7PdzKIo",
    authDomain: "waktu-sembahyang-brunei.firebaseapp.com",
    databaseURL: "https://waktu-sembahyang-brunei.firebaseio.com",
    projectId: "waktu-sembahyang-brunei",
    storageBucket: "waktu-sembahyang-brunei.appspot.com",
    messagingSenderId: "754965478900"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
